.. index::
   single: Test coverage

.. _test-coverage:

littlegrid test coverage report
===============================

.. note:: littlegrid has an extensive set of unit and integration/functional, test coverage of littlegrid is currently over 98%.

`Test coverage <test-coverage/|version|/index.html>`_
