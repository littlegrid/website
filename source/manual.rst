User manual
===========

What?
-----
littlegrid is a simple and easy to use framework that enables Coherence Developers to run
multiple Coherence cluster members in a single JVM.


Why?
----
Running multiple cluster members in a JVM is a **highly effective** way of performing
integration testing (sometimes called functional testing) of your Coherence code, such
as testing the functionality of your entry processors, cache stores and much, much more..

Running your Coherence functional tests in-process enables them to run faster because there is
just a single process to start and also in-process allows you to use standard local
debugging - this is a real productivity boost, allowing you to step through your Coherence
related code.

How?
----
To use littlegrid you use have to include the JAR and then typically just a few lines of code!
Here's an example of having two cache servers (storage-enabled servers) and then connecting as
a storage-disabled client.

.. code-block:: java

    ClusterMemberGroup memberGroup = ClusterMemberGroupUtils.newBuilder()
        .setStorageEnabledCount(2)
        .buildAndConfigureForStorageDisabledClient();

    NamedCache cache = CacheFactory.getCache("test");


Contents:

.. toctree::
   :maxdepth: 2

   list-of-examples
   website-updates
   introduction
   features
   who-is-using
   about
   getting-started
   getting-littlegrid
   using-extend-proxy
   faster-tests
   fast-start-join-timeout
   no-coding-needed
   mini-cluster-for-extend-clients
   failover-testing
   multiple-autonomous-clusters
   maven-best-practice
   continuous-integration
   builder-to-value-mapping
   builder-to-system-property-mapping
   reusable-member-group
   sample-exception-report
   javadoc
   test-coverage
   faq
   support
   license
   release-notes
