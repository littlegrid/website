.. _about:

About
=====

littlegrid is developed and actively maintained by Jon Hall (``jon at littlegrid dot org``).  littlegrid
incorporates many ideas and suggestions from other Coherence Consultants and is actively used
by Coherence Consultants because it is easy to use and provides the features required on typical
Coherence projects.

`Jon Hall on LinkedIn <http://uk.linkedin.com/in/jhall>`_

Please feel free to drop me an email and let me know what you think about littlegrid :-)

.. image:: _static/bitbucket-icon.png

.. image:: http://www.jetbrains.com/idea/opensource/img/all/banners/idea468x60_blue.gif


*Credit to Jonathan Knight (JK) and Andrew Wilson for original idea of using a separate
class-loader for each Coherence cluster member for the purposes of in-process testing.*

*Website developed and generated using Sphinx*

*Landing page look and free from Dropwizard*
