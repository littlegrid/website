.. index::
   single: JUnit; Extend example
   single: Extend; JUnit example

.. _using-extend-proxy:

Using Extend proxy
==================

If you haven't already had a look, the :ref:`getting-started` is the best place to start looking at littlegrid.

1) This example shows you how to run two storage-enabled members, an Extend proxy server and connect
as an Extend client - all in a single JVM using littlegrid.  Save the below class in a file
called: SimpleExtendIntegrationTest.java

.. code-block:: java

    import com.tangosol.net.CacheFactory;
    import com.tangosol.net.NamedCache;
    import com.tangosol.util.aggregator.Count;
    import com.tangosol.util.filter.AlwaysFilter;
    import org.junit.AfterClass;
    import org.junit.Before;
    import org.junit.BeforeClass;
    import org.junit.Test;
    import org.littlegrid.ClusterMemberGroup;
    import org.littlegrid.ClusterMemberGroupUtils;

    import java.util.Collections;

    import static junit.framework.Assert.assertEquals;

    /**
     * Simple Extend littlegrid integration tests.
     */
    public class SimpleExtendIntegrationTest {
        private static final int NUMBER_OF_ITEMS = 250;
        private static ClusterMemberGroup memberGroup;

        private final NamedCache cache = CacheFactory.getCache("simple-extend-example");


        /**
         * Use BeforeClass to start the cluster up before any of the tests run - this ensures
         * we only have the start-up delay only once.
         *
         * Note: apart from starting up and shutting down littlegrid, its code and API shouldn't
         * really be in any of your tests, unless you want to perform a stop or shutdown of a
         * particular member for failover testing.
         */
        @BeforeClass
        public static void beforeTests() {
            // Build the cluster
            //
            // Note: use of the fast-start option and log level set to 6, so that we see the
            // interesting Extend logging of client connections.
            //
            memberGroup = ClusterMemberGroupUtils.newBuilder()
                    .setStorageEnabledCount(2)
                    .setCacheConfiguration("simple-extend-cache-config.xml")
                    .setExtendProxyCount(1)
                    .setClientCacheConfiguration("simple-extend-client-cache-config.xml")
                    .setFastStartJoinTimeoutMilliseconds(1000)
                    .setLogLevel(6)
                    .buildAndConfigureForExtendClient();
        }

        /**
         * Shutdown the cluster, this method also does a CacheFactory.shutdown() for the client
         * to ensure that leaves nicely as well.
         */
        @AfterClass
        public static void afterTests() {
            ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(memberGroup);
        }

        /**
         * For this example test we are clearing down the cache each time and repopulating it.
         */
        @Before
        public void beforeTest() {
            cache.clear();

            for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
                cache.putAll(Collections.singletonMap(i, i));
            }

            assertEquals(NUMBER_OF_ITEMS, cache.size());
        }

        @Test
        public void putGet() {
            cache.put("key", "value");

            assertEquals(NUMBER_OF_ITEMS + 1, cache.size());
        }

        @Test
        public void aggregateCount() {
            assertEquals(NUMBER_OF_ITEMS, cache.aggregate(AlwaysFilter.INSTANCE, new Count()));
        }
    }


2) Before running the test, you need the cache server (storage-enabled) and Extend proxy
configuration file.  Save the below cache configuration in a file called:
simple-extend-cache-config.xml

.. code-block:: xml

    <cache-config>
        <caching-scheme-mapping>
            <cache-mapping>
                <cache-name>*</cache-name>
                <scheme-name>simple-distributed-scheme</scheme-name>
            </cache-mapping>
        </caching-scheme-mapping>

        <caching-schemes>
            <distributed-scheme>
                    <scheme-name>simple-distributed-scheme</scheme-name>
                    <backing-map-scheme>
                        <local-scheme/>
                    </backing-map-scheme>
                    <autostart>true</autostart>
            </distributed-scheme>

            <!--
                littlegrid uses system properties to enable Extend and control the address and
                port number that the Extend proxy server runs on.

                Note: if you want to use different system property names, that is fine as littlegrid
                supports a mapping to your system property names.
            -->
            <proxy-scheme>
                <scheme-name>extend-tcp</scheme-name>
                <service-name>ExtendProxyServerService</service-name>
                <acceptor-config>
                    <tcp-acceptor>
                        <local-address>
                            <address system-property="tangosol.coherence.extend.address">127.x.2.3</address>
                            <port system-property="tangosol.coherence.extend.port">9099</port>
                        </local-address>
                    </tcp-acceptor>
                </acceptor-config>
                <autostart system-property="tangosol.coherence.extend.enabled">false</autostart>
            </proxy-scheme>
        </caching-schemes>
    </cache-config>


3) Finally you just need the Extend client cache configuration, save this in a file called:
simple-extend-client-cache-config.xml

.. code-block:: xml

    <cache-config>
        <caching-scheme-mapping>
            <cache-mapping>
                <cache-name>*</cache-name>
                <scheme-name>extend-tcp</scheme-name>
            </cache-mapping>
        </caching-scheme-mapping>

        <caching-schemes>
            <remote-cache-scheme>
                <scheme-name>extend-tcp</scheme-name>
                <service-name>ExtendProxyClientService</service-name>
                <initiator-config>
                    <tcp-initiator>
                        <remote-addresses>
                            <socket-address>
                                <address system-property="tangosol.coherence.extend.address">127.x.2.3</address>
                                <port system-property="tangosol.coherence.extend.port">9099</port>
                            </socket-address>
                        </remote-addresses>
                    </tcp-initiator>
                </initiator-config>
            </remote-cache-scheme>
        </caching-schemes>
    </cache-config>


4) Now just run the example, try it from your IDE :-)  What you'll notice is that littlegrid
has set all the system properties and ports numbers, you don't need to do anything special -
just concentrate on the Coherence bit and let littlegrid start and shutdown the cluster.

.. code-block:: bash

    JUnit version 4.8.2
    05-Apr-2012 23:22:31 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid-builder-override.properties' not found - no properties loaded
    05-Apr-2012 23:22:31 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid/littlegrid-builder-override.properties' not found - no properties loaded
    05-Apr-2012 23:22:31 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid-builder-system-property-mapping-override.properties' not found - no properties loaded
    05-Apr-2012 23:22:31 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid/littlegrid-builder-system-property-mapping-override.properties' not found - no properties loaded
    05-Apr-2012 23:22:31 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildClusterMembers
    INFO: *** LittleGrid about to start - Storage-enabled: 2, Extend proxy: 1, Storage-enabled Extend proxy: 0, Custom configured: 0, JMX monitor: 0 ***
    05-Apr-2012 23:22:31 org.littlegrid.impl.DefaultClusterMemberGroupBuilder getSystemPropertiesForTcmpClusterMember
    WARNING: Fast-start join timeout specified.  Note: the fast-start Coherence override file will now be configured to be used
    05-Apr-2012 23:22:31 org.littlegrid.impl.DefaultClusterMemberGroup outputStartAllMessages
    INFO: System properties to be set.: {com.sun.management.jmxremote=false, littlegrid.join.timeout.milliseconds=100, tangosol.coherence.cacheconfig=simple-extend-cache-config.xml, tangosol.coherence.cluster=LittleGridCluster, tangosol.coherence.distributed.localstorage=true, tangosol.coherence.extend.address=127.0.0.1, tangosol.coherence.localhost=127.0.0.1, tangosol.coherence.localport=20800, tangosol.coherence.log=stdout, tangosol.coherence.log.level=6, tangosol.coherence.management=none, tangosol.coherence.management.remote=true, tangosol.coherence.override=littlegrid/littlegrid-fast-start-coherence-override.xml, tangosol.coherence.role=DedicatedStorageEnabledMember, tangosol.coherence.tcmp.enabled=true, tangosol.coherence.ttl=0, tangosol.coherence.wka=127.0.0.1, tangosol.coherence.wka.port=20800}
    05-Apr-2012 23:22:31 org.littlegrid.impl.DefaultClusterMemberGroup outputStartAllMessages
    INFO: Max memory: 1733MB, current: 116MB, free memory: 113MB
    2012-04-05 23:22:32.108/0.681 Oracle Coherence 3.7.1.0 <Info> (thread=pool-1-thread-1, member=n/a): Loaded operational configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence.xml"
    2012-04-05 23:22:32.112/0.685 Oracle Coherence 3.7.1.0 <Info> (thread=pool-1-thread-1, member=n/a): Loaded operational overrides from "jar:file:/home/jhall/maven/repository/org/littlegrid/littlegrid/2.6.1/littlegrid-2.6.1.jar!/littlegrid/littlegrid-fast-start-coherence-override.xml"
    2012-04-05 23:22:32.117/0.690 Oracle Coherence 3.7.1.0 <D5> (thread=pool-1-thread-1, member=n/a): Optional configuration override "/custom-mbeans.xml" is not specified
    2012-04-05 23:22:32.118/0.691 Oracle Coherence 3.7.1.0 <D6> (thread=pool-1-thread-1, member=n/a): Loaded edition data from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/coherence-grid.xml"

    Oracle Coherence Version 3.7.1.0 Build 27797
     Grid Edition: Development mode
    Copyright (c) 2000, 2011, Oracle and/or its affiliates. All rights reserved.

    ....More Coherence start-up messages

    2012-04-05 23:22:36.511/5.084 Oracle Coherence GE 3.7.1.0 <Info> (thread=pool-2-thread-1, member=n/a): Started cluster Name=LittleGridCluster

    WellKnownAddressList(Size=1,
      WKA{Address=127.0.0.1, Port=20800}
      )

    MasterMemberSet(
      ThisMember=Member(Id=3, Timestamp=2012-04-05 23:22:36.099, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedExtendProxyMember)
      OldestMember=Member(Id=1, Timestamp=2012-04-05 23:22:32.944, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember)
      ActualMemberSet=MemberSet(Size=3
        Member(Id=1, Timestamp=2012-04-05 23:22:32.944, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember)
        Member(Id=2, Timestamp=2012-04-05 23:22:34.594, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember)
        Member(Id=3, Timestamp=2012-04-05 23:22:36.099, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedExtendProxyMember)
        )
      MemberId|ServiceVersion|ServiceJoined|MemberState
        1|3.7.1|2012-04-05 23:22:32.944|JOINED,
        2|3.7.1|2012-04-05 23:22:34.594|JOINED,
        3|3.7.1|2012-04-05 23:22:36.5|JOINED
      RecycleMillis=1200000
      RecycleSet=MemberSet(Size=0
        )
      )

    TcpRing{Connections=[2]}
    IpMonitor{AddressListSize=0}

    2012-04-05 23:22:36.551/5.124 Oracle Coherence GE 3.7.1.0 <D5> (thread=Invocation:Management, member=3): Service Management joined the cluster with senior service member 1
    2012-04-05 23:22:36.560/5.133 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=1): Member 3 joined Service Management with senior member 1
    2012-04-05 23:22:36.561/5.134 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member 3 joined Service Management with senior member 1
    2012-04-05 23:22:36.812/5.385 Oracle Coherence GE 3.7.1.0 <D5> (thread=DistributedCache, member=3): Service DistributedCache joined the cluster with senior service member 1
    2012-04-05 23:22:36.828/5.401 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member 3 joined Service DistributedCache with senior member 1
    2012-04-05 23:22:36.828/5.401 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=1): Member 3 joined Service DistributedCache with senior member 1
    2012-04-05 23:22:36.955/5.528 Oracle Coherence GE 3.7.1.0 <Info> (thread=Proxy:ExtendProxyServerService:TcpAcceptor:TcpProcessor, member=3): The specified local address "/127.0.0.1" is a loopback address; clients running on remote machines will not be able to connect to this TcpAcceptor
    2012-04-05 23:22:36.961/5.535 Oracle Coherence GE 3.7.1.0 <Info> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): TcpAcceptor now listening for connections on 127.0.0.1:20900
    2012-04-05 23:22:36.963/5.536 Oracle Coherence GE 3.7.1.0 <D5> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Started: TcpAcceptor{Name=Proxy:ExtendProxyServerService:TcpAcceptor, State=(SERVICE_STARTED), ThreadCount=0, Codec=Codec(Format=POF), Serializer=com.tangosol.io.DefaultSerializer, PingInterval=0, PingTimeout=30000, RequestTimeout=30000, SocketProvider=SystemSocketProvider, LocalAddress=[/127.0.0.1:20900], SocketOptions{LingerTimeout=0, KeepAliveEnabled=true, TcpDelayEnabled=false}, ListenBacklog=0, BufferPoolIn=BufferPool(BufferSize=2KB, BufferType=DIRECT, Capacity=Unlimited), BufferPoolOut=BufferPool(BufferSize=2KB, BufferType=DIRECT, Capacity=Unlimited)}
    2012-04-05 23:22:36.976/5.549 Oracle Coherence GE 3.7.1.0 <D5> (thread=Proxy:ExtendProxyServerService, member=3): Service ExtendProxyServerService joined the cluster with senior service member 3
    2012-04-05 23:22:36.978/5.551 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member 3 joined Service ExtendProxyServerService with senior member 3
    2012-04-05 23:22:36.979/5.552 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=1): Member 3 joined Service ExtendProxyServerService with senior member 3
    05-Apr-2012 23:22:36 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildClusterMembers
    INFO: Group of cluster member(s) started, member Ids: [1, 2, 3]
    05-Apr-2012 23:22:36 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildAndConfigureForExtendClient
    INFO: System properties set for client: {tangosol.coherence.cacheconfig=simple-extend-client-cache-config.xml, tangosol.coherence.distributed.localstorage=false, tangosol.coherence.extend.address=127.0.0.1, tangosol.coherence.extend.enabled=false, tangosol.coherence.extend.port=20900, tangosol.coherence.log=stdout, tangosol.coherence.log.level=6, tangosol.coherence.role=ExtendClient, tangosol.coherence.tcmp.enabled=false}
    .2012-04-05 23:22:37.192/5.765 Oracle Coherence 3.7.1.0 <Info> (thread=main, member=n/a): Loaded operational configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence.xml"
    2012-04-05 23:22:37.240/5.813 Oracle Coherence 3.7.1.0 <Info> (thread=main, member=n/a): Loaded operational overrides from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence-override-dev.xml"
    2012-04-05 23:22:37.240/5.813 Oracle Coherence 3.7.1.0 <D5> (thread=main, member=n/a): Optional configuration override "/tangosol-coherence-override.xml" is not specified
    2012-04-05 23:22:37.244/5.817 Oracle Coherence 3.7.1.0 <D5> (thread=main, member=n/a): Optional configuration override "/custom-mbeans.xml" is not specified
    2012-04-05 23:22:37.245/5.818 Oracle Coherence 3.7.1.0 <D6> (thread=main, member=n/a): Loaded edition data from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/coherence-grid.xml"

    Oracle Coherence Version 3.7.1.0 Build 27797
     Grid Edition: Development mode
    Copyright (c) 2000, 2011, Oracle and/or its affiliates. All rights reserved.

    2012-04-05 23:22:37.352/5.925 Oracle Coherence GE 3.7.1.0 <Info> (thread=main, member=n/a): Loaded cache configuration from "file:/home/jhall/work/littlegrid/getting-started/simple-extend-client-cache-config.xml"; this document does not refer to any schema definition and has not been validated.
    2012-04-05 23:22:37.909/6.482 Oracle Coherence GE 3.7.1.0 <D5> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Started: TcpInitiator{Name=ExtendProxyClientService:TcpInitiator, State=(SERVICE_STARTED), ThreadCount=0, Codec=Codec(Format=POF), Serializer=com.tangosol.io.DefaultSerializer, PingInterval=0, PingTimeout=30000, RequestTimeout=30000, ConnectTimeout=30000, SocketProvider=SystemSocketProvider, RemoteAddresses=[/127.0.0.1:20900], SocketOptions{LingerTimeout=0, KeepAliveEnabled=true, TcpDelayEnabled=false}}
    2012-04-05 23:22:37.912/6.485 Oracle Coherence GE 3.7.1.0 <D5> (thread=main, member=n/a): Connecting Socket to 127.0.0.1:20900
    2012-04-05 23:22:37.912/6.485 Oracle Coherence GE 3.7.1.0 <Info> (thread=main, member=n/a): Connected Socket to 127.0.0.1:20900
    2012-04-05 23:22:37.970/6.543 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor:TcpProcessor, member=3): Incoming BufferPool increased to 2048 bytes total
    2012-04-05 23:22:38.002/6.575 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Opened: TcpConnection(Id=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8, Open=true, Member(Id=0, Timestamp=2012-04-05 23:22:37.924, Address=127.0.0.1:0, MachineId=0, Location=site:,machine:jhall-t410,process:3405, Role=ExtendClient), LocalAddress=127.0.0.1:20900, RemoteAddress=127.0.0.1:58954)
    2012-04-05 23:22:38.002/6.575 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Outgoing BufferPool increased to 2048 bytes total
    2012-04-05 23:22:38.018/6.591 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Opened: TcpConnection(Id=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8, Open=true, Member(Id=0, Timestamp=2012-04-05 23:22:37.924, Address=127.0.0.1:0, MachineId=0, Location=site:,machine:jhall-t410,process:3405, Role=ExtendClient), LocalAddress=127.0.0.1:58954, RemoteAddress=127.0.0.1:20900)
    2012-04-05 23:22:38.021/6.594 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Opened: Channel(Id=1043301565, Open=true, Connection=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8)
    2012-04-05 23:22:38.025/6.598 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Opened: Channel(Id=1043301565, Open=true, Connection=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8)
    2012-04-05 23:22:38.132/6.705 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Opened: Channel(Id=2853869, Open=true, Connection=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8)
    2012-04-05 23:22:38.138/6.711 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Opened: Channel(Id=2853869, Open=true, Connection=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8)
    2012-04-05 23:22:40.988/9.561 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Closed: Channel(Id=2853869, Open=false)
    2012-04-05 23:22:40.988/9.561 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Closed: Channel(Id=1043301565, Open=false)
    2012-04-05 23:22:40.989/9.562 Oracle Coherence GE 3.7.1.0 <D6> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Closed: TcpConnection(Id=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8, Open=false, Member(Id=0, Timestamp=2012-04-05 23:22:37.924, Address=127.0.0.1:0, MachineId=0, Location=site:,machine:jhall-t410,process:3405, Role=ExtendClient), LocalAddress=127.0.0.1:58954, RemoteAddress=127.0.0.1:20900)
    2012-04-05 23:22:40.991/9.564 Oracle Coherence GE 3.7.1.0 <D5> (thread=ExtendProxyClientService:TcpInitiator, member=n/a): Stopped: TcpInitiator{Name=ExtendProxyClientService:TcpInitiator, State=(SERVICE_STOPPED), ThreadCount=0, Codec=Codec(Format=POF), Serializer=com.tangosol.io.DefaultSerializer, PingInterval=0, PingTimeout=30000, RequestTimeout=30000, ConnectTimeout=30000, SocketProvider=SystemSocketProvider, RemoteAddresses=[/127.0.0.1:20900], SocketOptions{LingerTimeout=0, KeepAliveEnabled=true, TcpDelayEnabled=false}}
    2012-04-05 23:22:40.996/9.569 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Closed: Channel(Id=2853869, Open=false)
    2012-04-05 23:22:40.996/9.569 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Closed: Channel(Id=1043301565, Open=false)
    2012-04-05 23:22:40.997/9.570 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=3): Closed: TcpConnection(Id=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8, Open=false, Member(Id=0, Timestamp=2012-04-05 23:22:37.924, Address=127.0.0.1:0, MachineId=0, Location=site:,machine:jhall-t410,process:3405, Role=ExtendClient), LocalAddress=127.0.0.1:20900, RemoteAddress=127.0.0.1:58954)
    2012-04-05 23:22:40.998/9.571 Oracle Coherence GE 3.7.1.0 <D6> (thread=Proxy:ExtendProxyServerService:TcpAcceptor:TcpProcessor, member=3): Released: TcpConnection(Id=0x0000013689C087B2C0A801061570D97B1AC71CF8D69EC43DC733F004AD66F6B8, Open=false, Member(Id=0, Timestamp=2012-04-05 23:22:37.924, Address=127.0.0.1:0, MachineId=0, Location=site:,machine:jhall-t410,process:3405, Role=ExtendClient), LocalAddress=127.0.0.1:20900, RemoteAddress=127.0.0.1:58954)
    05-Apr-2012 23:22:40 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Restoring system properties back to their original state before member group started
    05-Apr-2012 23:22:40 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Shutting down '3' cluster member(s) in group
    2012-04-05 23:22:41.001/9.574 Oracle Coherence GE 3.7.1.0 <D5> (thread=Invocation:Management, member=n/a): Service Management left the cluster
    2012-04-05 23:22:41.004/9.577 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member 1 left service Management with senior member 2
    2012-04-05 23:22:41.005/9.578 Oracle Coherence GE 3.7.1.0 <D6> (thread=Invocation:Management, member=2): Service Management: sending ServiceConfig ConfigSync to all
    2012-04-05 23:22:41.004/9.577 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member 1 left service Management with senior member 2
    2012-04-05 23:22:41.008/9.581 Oracle Coherence GE 3.7.1.0 <D6> (thread=Invocation:Management, member=3): Service Management: received ServiceConfig ConfigSync from member 2 containing 1 entries
    2012-04-05 23:22:41.010/9.583 Oracle Coherence GE 3.7.1.0 <Info> (thread=DistributedCache, member=n/a): Remains to transfer before shutting down: 129 primary partitions, 128 backup partitions
    2012-04-05 23:22:41.042/9.615 Oracle Coherence GE 3.7.1.0 &ltD4> (thread=DistributedCache, member=2): Asking member 1 for 129 primary partitions
    2012-04-05 23:22:41.110/9.683 Oracle Coherence GE 3.7.1.0 <D5> (thread=DistributedCache, member=n/a): 4> Transferring primary PartitionSet{128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256} to member 2 requesting 129
    2012-04-05 23:22:41.212/9.785 Oracle Coherence GE 3.7.1.0 <D5> (thread=DistributedCache, member=n/a): Service DistributedCache left the cluster
    2012-04-05 23:22:41.214/9.787 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member 1 left service DistributedCache with senior member 2
    2012-04-05 23:22:41.215/9.788 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member 1 left service DistributedCache with senior member 2
    2012-04-05 23:22:41.215/9.788 Oracle Coherence GE 3.7.1.0 <D6> (thread=DistributedCache, member=2): Service DistributedCache: sending ServiceConfig ConfigSync to all
    2012-04-05 23:22:41.219/9.792 Oracle Coherence GE 3.7.1.0 <D6> (thread=DistributedCache, member=2): Service DistributedCache: sending PartitionConfig ConfigSync to all
    2012-04-05 23:22:41.220/9.793 Oracle Coherence GE 3.7.1.0 <D6> (thread=DistributedCache, member=3): Service DistributedCache: received ServiceConfig ConfigSync from member 2 containing 2 entries
    2012-04-05 23:22:41.224/9.797 Oracle Coherence GE 3.7.1.0 <D6> (thread=DistributedCache, member=3): Service DistributedCache: received PartitionConfig ConfigSync from member 2 containing 257 entries
    2012-04-05 23:22:41.236/9.809 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=n/a): Service Cluster left the cluster
    2012-04-05 23:22:41.238/9.811 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): TcpRing disconnected from Member(Id=1, Timestamp=2012-04-05 23:22:32.944, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) due to a peer departure; removing the member.
    2012-04-05 23:22:41.239/9.812 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=2): Member(Id=1, Timestamp=2012-04-05 23:22:41.238, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) left Cluster with senior member 2
    2012-04-05 23:22:41.241/9.814 Oracle Coherence GE 3.7.1.0 <D6> (thread=Cluster, member=n/a): TcpRing connecting to Member(Id=3, Timestamp=2012-04-05 23:22:36.099, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedExtendProxyMember)
    2012-04-05 23:22:41.242/9.815 Oracle Coherence GE 3.7.1.0 <D6> (thread=Cluster, member=n/a): TcpRing connected to Member(Id=3, Timestamp=2012-04-05 23:22:36.099, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedExtendProxyMember)
    2012-04-05 23:22:41.247/9.820 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): MemberLeft notification for Member(Id=1, Timestamp=2012-04-05 23:22:32.944, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) received from Member(Id=2, Timestamp=2012-04-05 23:22:34.594, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember)
    2012-04-05 23:22:41.247/9.820 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member(Id=1, Timestamp=2012-04-05 23:22:41.247, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) left Cluster with senior member 2
    2012-04-05 23:22:41.247/9.820 Oracle Coherence GE 3.7.1.0 <D5> (thread=Invocation:Management, member=n/a): Service Management left the cluster
    2012-04-05 23:22:41.249/9.822 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member 2 left service Management with senior member 3
    2012-04-05 23:22:41.249/9.822 Oracle Coherence GE 3.7.1.0 <D6> (thread=Invocation:Management, member=3): Service Management: sending ServiceConfig ConfigSync to all
    2012-04-05 23:22:41.252/9.825 Oracle Coherence GE 3.7.1.0 <D5> (thread=DistributedCache, member=n/a): Service DistributedCache left the cluster
    2012-04-05 23:22:41.252/9.825 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member 2 left service DistributedCache with senior member 3
    2012-04-05 23:22:41.252/9.825 Oracle Coherence GE 3.7.1.0 <D6> (thread=DistributedCache, member=3): Service DistributedCache: sending ServiceConfig ConfigSync to all
    2012-04-05 23:22:41.263/9.836 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=n/a): Service Cluster left the cluster
    2012-04-05 23:22:41.264/9.837 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): TcpRing disconnected from Member(Id=2, Timestamp=2012-04-05 23:22:34.594, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) due to a peer departure; removing the member.
    2012-04-05 23:22:41.265/9.838 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=3): Member(Id=2, Timestamp=2012-04-05 23:22:41.264, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:3405, Role=DedicatedStorageEnabledMember) left Cluster with senior member 3
    2012-04-05 23:22:41.267/9.840 Oracle Coherence GE 3.7.1.0 <D5> (thread=Invocation:Management, member=n/a): Service Management left the cluster
    2012-04-05 23:22:41.272/9.845 Oracle Coherence GE 3.7.1.0 <D5> (thread=DistributedCache, member=n/a): Service DistributedCache left the cluster
    2012-04-05 23:22:41.274/9.847 Oracle Coherence GE 3.7.1.0 <D5> (thread=Proxy:ExtendProxyServerService:TcpAcceptor, member=n/a): Stopped: TcpAcceptor{Name=Proxy:ExtendProxyServerService:TcpAcceptor, State=(SERVICE_STOPPED), ThreadCount=0, Codec=Codec(Format=POF), Serializer=com.tangosol.io.DefaultSerializer, PingInterval=0, PingTimeout=30000, RequestTimeout=30000, SocketProvider=SystemSocketProvider, LocalAddress=[/127.0.0.1:20900], SocketOptions{LingerTimeout=0, KeepAliveEnabled=true, TcpDelayEnabled=false}, ListenBacklog=0, BufferPoolIn=BufferPool(BufferSize=2KB, BufferType=DIRECT, Capacity=Unlimited), BufferPoolOut=BufferPool(BufferSize=2KB, BufferType=DIRECT, Capacity=Unlimited)}
    2012-04-05 23:22:41.276/9.849 Oracle Coherence GE 3.7.1.0 <D5> (thread=Proxy:ExtendProxyServerService, member=n/a): Service ExtendProxyServerService left the cluster
    2012-04-05 23:22:41.452/10.025 Oracle Coherence GE 3.7.1.0 <D5> (thread=Cluster, member=n/a): Service Cluster left the cluster
    05-Apr-2012 23:22:41 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Group of cluster member(s) shutdown
    Time: 9.913

    OK (2 tests)
