.. _features:

Features
========

Here's a brief overview of some of littlegrid's features.

* Stop cluster members - this causes them to leave as if they had been killed, useful for testing failover
* Fast-start option - helps start the cluster even faster
* Access the child-first class loader containing the Coherence cluster member - this enables you to control or load classes and objects directly within any of the class loaders running Coherence members
* Control the class-path used by the Coherence cluster members or easily exclude JARs from the default class-path.
* Exception reporter - produces a report-style output in the event of the cluster not starting, this helps with trouble-shooting the problem.
* Extend the littlegrid default cluster member to easily provide hooks for your own life-cycle methods (before or after the cluster member starts and before or after it shuts down)
* Build the cluster using a simple and concise property file
* Provide an override file to selectively override properties - useful if you need to precisely control which ports are used for different build plans on your CI (Continuous Integration) server
