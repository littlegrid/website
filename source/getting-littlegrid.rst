.. index::
   single: Download; Getting littlegrid JAR
   single: Source code; Getting the littlegrid source code

.. _getting-littlegrid:

Getting littlegrid
==================

You can get littlegrid in a number of ways - if you use Maven, then it is available from Maven central,
alternatively you can download the littlegrid JAR using the link on the left hand side.

* `Download latest stable release <http://search.maven.org/remotecontent?filepath=org/littlegrid/littlegrid/|version|/littlegrid-|version|.jar>`_
* Using Maven and :ref:`maven-best-practice`
* `Browse the latest development source <https://bitbucket.org/littlegrid/littlegrid-coherence-testsupport/src>`_

.. note:: littlegrid doesn't have any additional third-party dependencies - you just need Coherence and littlegrid to get up and running.
