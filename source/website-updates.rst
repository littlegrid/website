.. index::
   single: Website; Updates - recent changes to pages

Website updates
===============

The below outlines the most recent major changes to the website, thus allowing visitors to easily determine
what changes have occurred since their last visit.

.. note:: Contact 'help at littlegrid dot org' to suggest website changes or corrections.  If you want to contribute, then let us know - any help is welcome :-)

2014-07-17 Added a new sector of littlegrid users, see :ref:`who-is-using`.

2013-12-15 Updated configuring and controlling littlegrid, see :ref:`builder-to-value-mapping`.

2013-12-15 Start work on :ref:`replacing-default-cache-server`.

2013-12-15 Updated page about making tests faster :ref:`faster-tests` and :ref:`fast-start-join-timeout`.

2013-11-25 Started work on mini-cluster for Extend :ref:`mini-cluster-for-extend-clients`

2013-11-23 Website went live on faster hosted environment using littlegrid.net domain.

2013-11 Updated content on :ref:`builder-to-system-property-mapping`.