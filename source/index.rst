.. title:: Home

.. raw:: html

    <div class="hero-unit">

#########################################################
littlegrid - helping developers test their Coherence code
#########################################################

littlegrid is a **small** and **simple** framework that enables you to start
multiple Coherence cluster members in a single JVM - this is a **highly effective** way of
developing Coherence tests, because the cluster is **quick to start** and you can
**debug locally**.

littlegrid quickly lets you try out ideas in your **IDE** and makes it easy for you to automate
your Coherence tests for running on your **Continuous Integration** (CI) build servers.

*Developing systems using Oracle Coherence?  ...then read on!*

**Coherence + littlegrid = develop faster :-)**

.. toctree::
   :maxdepth: 1

   getting-started
   manual

.. raw:: html

    </div>
