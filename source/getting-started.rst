.. _getting-started:

Getting started
===============

* :ref:`simple-standalone-example`
* :ref:`simple-junit-example`

.. _simple-standalone-example:

Simple standalone example
-------------------------

This getting started will only take a couple of minutes to complete, but by the end you would have
run an entire Coherence cluster in a single JVM and performed operations against a cache (if you
want to simply fast-forward, :ref:`simple-junit-example` here's what a JUnit test looks like)

Before getting started, you'll need to download
`Coherence <http://www.oracle.com/technetwork/middleware/coherence/downloads/index.html>`_ and
:ref:`getting-littlegrid` littlegrid.  Once you've downloaded the JARs, you'll need to create
a simple Java application - you can use your favourite IDE or even Notepad if you want :-).

1) Save the below program into a file called SimplelittlegridExample.java

.. code-block:: java

    import com.tangosol.net.CacheFactory;
    import com.tangosol.net.NamedCache;
    import org.littlegrid.ClusterMemberGroupUtils;
    import org.littlegrid.ClusterMemberGroup;

    public class SimplelittlegridExample {
        public static void main(String[] args) {
            final String key = "123";

            ClusterMemberGroup memberGroup = null;

            try {
                memberGroup = ClusterMemberGroupUtils.newBuilder()
                        .setStorageEnabledCount(2)
                        .buildAndConfigureForStorageDisabledClient();

                final NamedCache cache = CacheFactory.getCache("test");
                cache.put(key, "hello");

                System.out.println(cache.get(key));
            } finally {
                ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(memberGroup);
            }
        }
    }

2) From the command-line compile the class:

``javac -cp C:\coherence\lib\coherence.jar;C:\littlegrid\littlegrid-|version|.jar;. SimplelittlegridExample.java``

.. note:: Change the path of Coherence and littlegrid to match where you have the JAR files.


3) Then simply run the application from the command-line where you compiled the class:

``java -cp C:\coherence\lib\coherence.jar;C:\littlegrid\littlegrid-|version|.jar;. SimplelittlegridExample``

You should see some output similar to the below - essentially, two storage-enabled members (often
called cache servers) are started and then the application joins the cluster as a storage-disabled
client where it then performs a put and a get against a named cache called 'test':

Notice how littlegrid has done the following:

* Given the members sensible rolenames, such as DedicatedStorageEnabledMember
* Ensured that WKA (Well Known Addresses) are being used and that the network traffic is restricted to this machine - this prevents accidental clustering
* Used different port numbers to avoid clustering with vanilla Coherence instances you might have already running

.. code-block:: bash

    05-Apr-2012 21:58:30 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid-builder-override.properties' not found - no properties loaded
    05-Apr-2012 21:58:30 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid/littlegrid-builder-override.properties' not found - no properties loaded
    05-Apr-2012 21:58:30 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid-builder-system-property-mapping-override.properties' not found - no properties loaded
    05-Apr-2012 21:58:30 org.littlegrid.support.PropertiesUtils loadProperties
    INFO: File 'littlegrid/littlegrid-builder-system-property-mapping-override.properties' not found - no properties loaded
    05-Apr-2012 21:58:30 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildClusterMembers
    INFO: *** LittleGrid about to start - Storage-enabled: 2, Extend proxy: 0, Storage-enabled Extend proxy: 0, Custom configured: 0, JMX monitor: 0 ***
    05-Apr-2012 21:58:30 org.littlegrid.impl.DefaultClusterMemberGroup outputStartAllMessages
    INFO: System properties to be set.: {com.sun.management.jmxremote=false, tangosol.coherence.cluster=LittleGridCluster, tangosol.coherence.distributed.localstorage=true, tangosol.coherence.extend.address=127.0.0.1, tangosol.coherence.localhost=127.0.0.1, tangosol.coherence.localport=20800, tangosol.coherence.log=stdout, tangosol.coherence.log.level=3, tangosol.coherence.management=none, tangosol.coherence.management.remote=true, tangosol.coherence.role=DedicatedStorageEnabledMember, tangosol.coherence.tcmp.enabled=true, tangosol.coherence.ttl=0, tangosol.coherence.wka=127.0.0.1, tangosol.coherence.wka.port=20800}
    05-Apr-2012 21:58:30 org.littlegrid.impl.DefaultClusterMemberGroup outputStartAllMessages
    INFO: Max memory: 1733MB, current: 116MB, free memory: 114MB
    2012-04-05 21:58:31.044/1.020 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-1, member=n/a): Loaded operational configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence.xml"
    2012-04-05 21:58:31.145/1.121 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-1, member=n/a): Loaded operational overrides from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence-override-dev.xml"

    Oracle Coherence Version 3.7.1.0 Build 27797
     Grid Edition: Development mode
    Copyright (c) 2000, 2011, Oracle and/or its affiliates. All rights reserved.

    2012-04-05 21:58:31.747/1.723 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-1, member=n/a): Loaded cache configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/coherence-cache-config.xml"
    2012-04-05 21:58:32.371/2.347 Oracle Coherence GE 3.7.1.0 &lt;Warning&gt; (thread=pool-1-thread-1, member=n/a): Local address "127.0.0.1" is a loopback address; this cluster node will not connect to nodes located on different machines
    2012-04-05 21:58:35.994/5.970 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=Cluster, member=n/a): Created a new cluster "LittleGridCluster" with Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember, Edition=Grid Edition, Mode=Development, CpuCount=4, SocketCount=2) UID=0x7F00000100000136844D2F93EB9A5140
    2012-04-05 21:58:36.000/5.976 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-1, member=n/a): Started cluster Name=LittleGridCluster

    WellKnownAddressList(Size=1,
      WKA{Address=127.0.0.1, Port=20800}
      )

    MasterMemberSet(
      ThisMember=Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
      OldestMember=Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
      ActualMemberSet=MemberSet(Size=1
        Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
        )
      MemberId|ServiceVersion|ServiceJoined|MemberState
        1|3.7.1|2012-04-05 21:58:35.994|JOINED
      RecycleMillis=1200000
      RecycleSet=MemberSet(Size=0
        )
      )

    TcpRing{Connections=[]}
    IpMonitor{AddressListSize=0}

    2012-04-05 21:58:36.666/6.642 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-2, member=n/a): Loaded operational configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence.xml"
    2012-04-05 21:58:36.715/6.691 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-2, member=n/a): Loaded operational overrides from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence-override-dev.xml"

    Oracle Coherence Version 3.7.1.0 Build 27797
     Grid Edition: Development mode
    Copyright (c) 2000, 2011, Oracle and/or its affiliates. All rights reserved.

    2012-04-05 21:58:36.859/6.835 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-2, member=n/a): Loaded cache configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/coherence-cache-config.xml"
    2012-04-05 21:58:37.169/7.145 Oracle Coherence GE 3.7.1.0 &lt;Warning&gt; (thread=pool-1-thread-2, member=n/a): Local address "127.0.0.1" is a loopback address; this cluster node will not connect to nodes located on different machines
    2012-04-05 21:58:37.537/7.513 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=Cluster, member=n/a): This Member(Id=2, Timestamp=2012-04-05 21:58:37.352, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember, Edition=Grid Edition, Mode=Development, CpuCount=4, SocketCount=2) joined cluster "LittleGridCluster" with senior Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember, Edition=Grid Edition, Mode=Development, CpuCount=4, SocketCount=2)
    2012-04-05 21:58:37.749/7.725 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=pool-1-thread-2, member=n/a): Started cluster Name=LittleGridCluster

    WellKnownAddressList(Size=1,
      WKA{Address=127.0.0.1, Port=20800}
      )

    MasterMemberSet(
      ThisMember=Member(Id=2, Timestamp=2012-04-05 21:58:37.352, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
      OldestMember=Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
      ActualMemberSet=MemberSet(Size=2
        Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
        Member(Id=2, Timestamp=2012-04-05 21:58:37.352, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
        )
      MemberId|ServiceVersion|ServiceJoined|MemberState
        1|3.7.1|2012-04-05 21:58:32.723|JOINED,
        2|3.7.1|2012-04-05 21:58:37.74|JOINED
      RecycleMillis=1200000
      RecycleSet=MemberSet(Size=0
        )
      )

    TcpRing{Connections=[1]}
    IpMonitor{AddressListSize=0}

    05-Apr-2012 21:58:38 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildClusterMembers
    INFO: Group of cluster member(s) started, member Ids: [1, 2]
    05-Apr-2012 21:58:38 org.littlegrid.impl.DefaultClusterMemberGroupBuilder buildAndConfigureForStorageDisabledClient
    INFO: System properties set for client: {com.sun.management.jmxremote=false, tangosol.coherence.cluster=LittleGridCluster, tangosol.coherence.distributed.localstorage=false, tangosol.coherence.extend.address=127.0.0.1, tangosol.coherence.extend.enabled=false, tangosol.coherence.localhost=127.0.0.1, tangosol.coherence.localport=20800, tangosol.coherence.log=stdout, tangosol.coherence.log.level=3, tangosol.coherence.management=none, tangosol.coherence.management.remote=true, tangosol.coherence.role=StorageDisabledClient, tangosol.coherence.tcmp.enabled=true, tangosol.coherence.ttl=0, tangosol.coherence.wka=127.0.0.1, tangosol.coherence.wka.port=20800}
    2012-04-05 21:58:38.526/8.502 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=main, member=n/a): Loaded operational configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence.xml"
    2012-04-05 21:58:38.573/8.549 Oracle Coherence 3.7.1.0 &lt;Info&gt; (thread=main, member=n/a): Loaded operational overrides from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/tangosol-coherence-override-dev.xml"

    Oracle Coherence Version 3.7.1.0 Build 27797
     Grid Edition: Development mode
    Copyright (c) 2000, 2011, Oracle and/or its affiliates. All rights reserved.

    2012-04-05 21:58:38.705/8.681 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=main, member=n/a): Loaded cache configuration from "jar:file:/opt/coherence/coherence-3.7.1.0/lib/coherence.jar!/coherence-cache-config.xml"
    2012-04-05 21:58:39.109/9.085 Oracle Coherence GE 3.7.1.0 &lt;Warning&gt; (thread=main, member=n/a): Local address "127.0.0.1" is a loopback address; this cluster node will not connect to nodes located on different machines
    2012-04-05 21:58:39.474/9.450 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=Cluster, member=n/a): This Member(Id=3, Timestamp=2012-04-05 21:58:39.289, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=StorageDisabledClient, Edition=Grid Edition, Mode=Development, CpuCount=4, SocketCount=2) joined cluster "LittleGridCluster" with senior Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember, Edition=Grid Edition, Mode=Development, CpuCount=4, SocketCount=2)
    2012-04-05 21:58:39.705/9.681 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=main, member=n/a): Started cluster Name=LittleGridCluster

    WellKnownAddressList(Size=1,
      WKA{Address=127.0.0.1, Port=20800}
      )

    MasterMemberSet(
      ThisMember=Member(Id=3, Timestamp=2012-04-05 21:58:39.289, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=StorageDisabledClient)
      OldestMember=Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
      ActualMemberSet=MemberSet(Size=3
        Member(Id=1, Timestamp=2012-04-05 21:58:32.723, Address=127.0.0.1:20800, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
        Member(Id=2, Timestamp=2012-04-05 21:58:37.352, Address=127.0.0.1:20802, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=DedicatedStorageEnabledMember)
        Member(Id=3, Timestamp=2012-04-05 21:58:39.289, Address=127.0.0.1:20804, MachineId=60314, Location=site:,machine:localhost,process:27512, Role=StorageDisabledClient)
        )
      MemberId|ServiceVersion|ServiceJoined|MemberState
        1|3.7.1|2012-04-05 21:58:32.723|JOINED,
        2|3.7.1|2012-04-05 21:58:37.352|JOINED,
        3|3.7.1|2012-04-05 21:58:39.693|JOINED
      RecycleMillis=1200000
      RecycleSet=MemberSet(Size=0
        )
      )

    TcpRing{Connections=[2]}
    IpMonitor{AddressListSize=0}

    hello
    05-Apr-2012 21:58:40 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Restoring system properties back to their original state before member group started
    05-Apr-2012 21:58:40 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Shutting down '2' cluster member(s) in group
    2012-04-05 21:58:40.081/10.057 Oracle Coherence GE 3.7.1.0 &lt;Info&gt; (thread=DistributedCache, member=n/a): Remains to transfer before shutting down: 129 primary partitions, 128 backup partitions
    05-Apr-2012 21:58:40 org.littlegrid.impl.DefaultClusterMemberGroup shutdownAll
    INFO: Group of cluster member(s) shutdown

.. index::
   single: JUnit; Simple example
   single: Getting started; Simple example

.. _simple-junit-example:

Simple JUnit example
--------------------

Here's a JUnit example - note the use of ``@BeforeClass`` and ``@AfterClass``

.. code-block:: java

    public final class SimpleTest {
        private static ClusterMemberGroup memberGroup;

        @BeforeClass
        public static void beforeTests() {
            memberGroup = ClusterMemberGroupUtils.newBuilder()
                    .setStorageEnabledCount(2)
                    .buildAndConfigureForStorageDisabledClient();
        }

        @AfterClass
        public static void afterTests() {
            ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(memberGroup);
        }

        @Test
        public void simpleExample() {
            final NamedCache cache = CacheFactory.getCache("test");
            cache.put("key", "hello");

            assertEquals(1, cache.size());
        }
    }

The above simple test actually does quite a bit with just a couple of lines of code, firstly it starts
to cache servers (storage-enabled cluster members) and then the test itself joins the cluster as a
storage-disabled client where it then performs simple actions against the test cache.

.. note:: Notice how the littlegrid specific code is both fluent (i.e. simple to understand) and also there isn't much of it - littlegrid is fairly non-intrusive to introduce into an existing code-base.
