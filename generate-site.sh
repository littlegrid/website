#!/bin/sh

rm -rf ./build/html/

make html

find ./build/html/ -type f -iname "*.html" -exec sed -i.bak 's/|version|/2.15.2/g' "{}" +;

rm ./build/html/*.bak

